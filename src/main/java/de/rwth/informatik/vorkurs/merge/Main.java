package de.rwth.informatik.vorkurs.merge;

public class Main {
    public static void main(String[] args) {
        hello("World", 3);
        hello("Alice", 2);
        System.out.println("-----------");
    }

    private static void hello(String target, int times) {
        for (int i = 0; i < times; i++) {
            System.out.println("Hello " + target + "!");
        }
    }
}
